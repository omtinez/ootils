/** Helper module that contains mail-related methods */
export declare module Mail {
    /**
     * Sends an HTML email
     * @param mg_key private mailgun key
     * @param mg_domain verified domain for mailgun
     * @param from name <email>
     * @param recipient comma-separated email addresses
     * @param subject email subject
     * @param content raw HTML email content
     */
    function send(mg_key: string, mg_domain: string, from: string, recipient: string, subject: string, content: string): Promise<void>;
}
