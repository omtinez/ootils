/** Helper module that contains data-related methods */
export declare module DataMunging {
    /**
     * Wrapper that promisifies `csv-parse`
     * @param data contents in CSV form
     * @param opts options to be passed to `csv-parse()`
     */
    function loadcsv(data: string, opts?: {
        [key: string]: any;
    }): Promise<any[]>;
    /**
     * Wrapper that reads a file and then loads it assuming CSV format
     * @param fname path of the CSV file to be read
     * @param opts options to be passed to `csv-parse()`
     */
    function readcsv(fname: string, opts?: {
        [key: string]: any;
    }): Promise<any[]>;
    /**
     * Shuffles an array in place using Fisher-Yates Shuffle
     * @param arr array to be shuffled
     */
    function shuffle(arr: any[]): any[];
    /**
     * Produces an array of integers ranging beween `[start, end)`
     * @param end last index of the array, not inclusive
     * @param start start index of the array, inclusive
     * @return array of [start..end)
     */
    function range(end: number, start?: number): number[];
    /**
     * Produces an array resulting from applying `func` to each item of the given array. Each call
     * to `func` can yield zero, one or more values, and the resulting array will be flattened.
     * @param arr list of items to iterate over
     * @param func function that converts each item into a new item, a list of new items, or `null`
     */
    function produce<T>(arr: T[], func: (item: T, index?: number, arr?: T[]) => any | any[]): any[];
}
