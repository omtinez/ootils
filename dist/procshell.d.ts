/** Helper module that contains process and shell-related methods */
export declare module ProcShell {
    /** Wrapper that promisifies `which` */
    function which(command: string): Promise<string>;
    /** Wrapper that promisifies `spawn` */
    function spawn(command: string, args?: string[], opts?: {
        [key: string]: any;
    }): Promise<void>;
    /** Wrapper that promisifies `exec` */
    function exec(command: string, opts?: {
        [key: string]: any;
    }): Promise<any>;
    /** Wrapper that promisifies `execFile` */
    function file(command: string, args?: string[], opts?: {
        [key: string]: any;
    }): Promise<any>;
}
