"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ootils_1 = require("../ootils");
const assert = require("assert");
describe('procshell', () => {
    describe('spawn', () => {
        it('creates, deletes file', done => {
            const outfile = `test-${Math.floor(Math.random() * 9999)}.txt`;
            const msg = 'Hello World ' + Math.floor(Math.random() * 9999);
            ootils_1.ProcShell.spawn('echo', [msg, '>', outfile]).then(() => {
                ootils_1.ProcShell.spawn('rm', [outfile]).then(done);
            });
        });
    });
    describe('exec', () => {
        it('creates, reads, deletes file', done => {
            const catcher = (err) => assert.equal(err.message, null);
            const outfile = `test-${Math.floor(Math.random() * 9999)}.txt`;
            const msg = 'Hello World ' + Math.floor(Math.random() * 9999);
            ootils_1.ProcShell.exec(['echo', msg, '>', outfile].join(' ')).then(() => {
                return ootils_1.ProcShell.exec(['cat', outfile].join(' '));
            }).then(txt => {
                assert.equal(msg, txt);
                return ootils_1.ProcShell.exec(['rm', outfile].join(' '));
            }).then(done);
        });
    });
});
