"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ootils_1 = require("../ootils");
const assert = require("assert");
describe('datamunging', () => {
    describe('readcsv', () => {
        it('todo', done => {
            done();
        });
    });
    describe('#array', () => {
        it('produce', done => {
            const tmp = [[1, 2], [3, 4], [5, 6], [7, 8]];
            const arr1 = ootils_1.DataMunging.produce(tmp, x => x);
            const arr2 = [1, 2, 3, 4, 5, 6, 7, 8];
            assert.deepEqual(arr1, arr2);
            done();
        });
    });
});
