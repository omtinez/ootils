"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ootils_1 = require("../ootils");
describe('mail', () => {
    describe('send', () => {
        it('send dummy email #skipci', () => __awaiter(this, void 0, void 0, function* () {
            const domain = 'mailer.omtinez.com';
            const token = 'key-qwertyuiopasdfghjklzxcvbnm123456';
            const ts = new Date().getTime();
            yield ootils_1.Mail.send(token, domain, 'Test <contact@omtinez.com>', 'omtinez@gmail.com', `(${ts}) ootils mail test`, `(${ts}) ootils mail test content`);
        }));
    });
});
