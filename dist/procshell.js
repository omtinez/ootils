"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const child_process = require("child_process");
const which_ = require("which");
/** Helper module that contains process and shell-related methods */
var ProcShell;
(function (ProcShell) {
    /** Wrapper that promisifies `which` */
    function which(command) {
        return new Promise((resolve, reject) => {
            which_(command, (err, path) => {
                if (err) {
                    reject(err);
                }
                else if (!path) {
                    reject(new Error(`${command} not found`));
                }
                else {
                    resolve(path);
                }
            });
        });
    }
    ProcShell.which = which;
    /** Wrapper that promisifies `spawn` */
    function spawn(command, args, opts = {}) {
        return new Promise((resolve, reject) => {
            ProcShell.which(command).then(command => {
                // Sensible defaults for options
                opts.cwd = opts.hasOwnProperty('cwd') ? opts.cwd : __dirname;
                opts.shell = opts.hasOwnProperty('shell') ? opts.shell : true;
                opts.stdio = opts.hasOwnProperty('stdio') ? opts.stdio : ['ipc'];
                const child = child_process.spawn(command, args, opts);
                child.on('close', code => {
                    if (code !== 0) {
                        reject(new Error(`"${command}" exited with non-zero code ${code}`));
                    }
                    else {
                        resolve();
                    }
                });
            }).catch(err => reject(err));
        });
    }
    ProcShell.spawn = spawn;
    /** Wrapper that promisifies `exec` */
    function exec(command, opts = {}) {
        return new Promise((resolve, reject) => {
            try {
                const opts_ = {
                    maxBuffer: opts.maxBuffer || 1024 * 500
                };
                const child = child_process.exec(command, opts_, (err, stdout, stderr) => {
                    if (err) {
                        reject(err);
                    }
                    else if (!opts.ignore_stderr && stderr) {
                        reject(new Error(stderr));
                    }
                    else {
                        resolve(stdout.trim());
                    }
                });
            }
            catch (ex) {
                reject(new Error(ex.message));
            }
        });
    }
    ProcShell.exec = exec;
    /** Wrapper that promisifies `execFile` */
    function file(command, args = [], opts = {}) {
        return new Promise((resolve, reject) => {
            try {
                const child = child_process.execFile(command, args, (err, stdout, stderr) => {
                    if (err) {
                        reject(err);
                    }
                    else if (!opts.ignore_stderr && stderr) {
                        reject(new Error(stderr));
                    }
                    else {
                        resolve(stdout.trim());
                    }
                });
            }
            catch (ex) {
                reject(new Error(ex.message));
            }
        });
    }
    ProcShell.file = file;
})(ProcShell = exports.ProcShell || (exports.ProcShell = {}));
