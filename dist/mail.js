"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Mailgun = require('mailgun').Mailgun;
/** Helper module that contains mail-related methods */
var Mail;
(function (Mail) {
    /**
     * Sends an HTML email
     * @param mg_key private mailgun key
     * @param mg_domain verified domain for mailgun
     * @param from name <email>
     * @param recipient comma-separated email addresses
     * @param subject email subject
     * @param content raw HTML email content
     */
    function send(mg_key, mg_domain, from, recipient, subject, content) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                const mg = new Mailgun(mg_key);
                mg.sendRaw('mailgun@' + mg_domain, recipient, [`From: ${from}`, `To: ${recipient}`, `Content-Type: text/html; charset=utf-8`,
                    `Subject: ${subject}`, '', `${content}`].join('\n'), (err) => {
                    if (err) {
                        reject(err);
                    }
                    else {
                        resolve();
                    }
                });
            });
        });
    }
    Mail.send = send;
})(Mail = exports.Mail || (exports.Mail = {}));
