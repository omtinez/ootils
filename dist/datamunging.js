"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const csv = require("csv-parse");
/** Helper module that contains data-related methods */
var DataMunging;
(function (DataMunging) {
    /**
     * Wrapper that promisifies `csv-parse`
     * @param data contents in CSV form
     * @param opts options to be passed to `csv-parse()`
     */
    function loadcsv(data, opts = { columns: true }) {
        return new Promise((resolve, reject) => {
            csv(data, opts, (err, data) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(data);
                }
            });
        });
    }
    DataMunging.loadcsv = loadcsv;
    /**
     * Wrapper that reads a file and then loads it assuming CSV format
     * @param fname path of the CSV file to be read
     * @param opts options to be passed to `csv-parse()`
     */
    function readcsv(fname, opts = { columns: true }) {
        return new Promise((resolve, reject) => {
            fs.readFile(fname, 'utf8', (err, data) => __awaiter(this, void 0, void 0, function* () {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(yield DataMunging.loadcsv(data, opts));
                }
            }));
        });
    }
    DataMunging.readcsv = readcsv;
    /**
     * Shuffles an array in place using Fisher-Yates Shuffle
     * @param arr array to be shuffled
     */
    function shuffle(arr) {
        for (let i = arr.length; i; i--) {
            let j = Math.floor(Math.random() * i);
            [arr[i - 1], arr[j]] = [arr[j], arr[i - 1]];
        }
        return arr;
    }
    DataMunging.shuffle = shuffle;
    /**
     * Produces an array of integers ranging beween `[start, end)`
     * @param end last index of the array, not inclusive
     * @param start start index of the array, inclusive
     * @return array of [start..end)
     */
    function range(end, start = 0) {
        const arr = [];
        for (let i = start; i < end; i++)
            arr.push(i);
        return arr;
    }
    DataMunging.range = range;
    /**
     * Produces an array resulting from applying `func` to each item of the given array. Each call
     * to `func` can yield zero, one or more values, and the resulting array will be flattened.
     * @param arr list of items to iterate over
     * @param func function that converts each item into a new item, a list of new items, or `null`
     */
    function produce(arr, func) {
        const res = [];
        arr.forEach((item, ix, arr) => {
            const val = func(item, ix, arr);
            if (typeof val === 'undefined' || val === null) {
                return;
            }
            else if (Array.isArray(val)) {
                res.push(...val);
            }
            else {
                res.push(val);
            }
        });
        return res;
    }
    DataMunging.produce = produce;
})(DataMunging = exports.DataMunging || (exports.DataMunging = {}));
