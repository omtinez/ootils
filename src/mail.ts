const Mailgun: any = require('mailgun').Mailgun;

/** Helper module that contains mail-related methods */
export module Mail {

    /**
     * Sends an HTML email
     * @param mg_key private mailgun key
     * @param mg_domain verified domain for mailgun
     * @param from name <email>
     * @param recipient comma-separated email addresses
     * @param subject email subject
     * @param content raw HTML email content
     */
    export async function send(mg_key: string, mg_domain: string, from: string, recipient: string, subject: string, content: string) {
        return new Promise<void>((resolve, reject) => {
            const mg: any = new Mailgun(mg_key);
            mg.sendRaw('mailgun@' + mg_domain, recipient,
                [`From: ${from}`, `To: ${recipient}`, `Content-Type: text/html; charset=utf-8`,
                `Subject: ${subject}`, '', `${content}`].join('\n'), (err: any) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
        });
    }
}