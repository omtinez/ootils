import * as fs from 'fs';
import * as csv from 'csv-parse';

/** Helper module that contains data-related methods */
export module DataMunging {

    /**
     * Wrapper that promisifies `csv-parse`
     * @param data contents in CSV form
     * @param opts options to be passed to `csv-parse()`
     */
    export function loadcsv(data: string, opts: {[key: string]: any} = {columns: true}): Promise<any[]> {
        return new Promise<any[]>((resolve, reject) => {
            csv(data, opts, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }

    /**
     * Wrapper that reads a file and then loads it assuming CSV format
     * @param fname path of the CSV file to be read
     * @param opts options to be passed to `csv-parse()`
     */
    export function readcsv(fname: string, opts: {[key: string]: any} = {columns: true}): Promise<any[]> {
        return new Promise<any[]>((resolve, reject) => {
            fs.readFile(fname, 'utf8', async (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(await DataMunging.loadcsv(data, opts));
                }
            });
        });
    }

    /**
     * Shuffles an array in place using Fisher-Yates Shuffle
     * @param arr array to be shuffled
     */
    export function shuffle(arr: any[]): any[] {
        for (let i = arr.length; i; i--) {
            let j = Math.floor(Math.random() * i);
            [arr[i - 1], arr[j]] = [arr[j], arr[i - 1]];
        }
        return arr;
    }

    /**
     * Produces an array of integers ranging beween `[start, end)`
     * @param end last index of the array, not inclusive
     * @param start start index of the array, inclusive
     * @return array of [start..end)
     */
    export function range(end: number, start: number = 0) {
        const arr: number[] = [];
        for (let i = start; i < end; i++) arr.push(i);
        return arr;
    }

    /**
     * Produces an array resulting from applying `func` to each item of the given array. Each call
     * to `func` can yield zero, one or more values, and the resulting array will be flattened.
     * @param arr list of items to iterate over
     * @param func function that converts each item into a new item, a list of new items, or `null`
     */
    export function produce<T>(arr: T[], func: (item: T, index?: number, arr?: T[]) => any | any[]): any[] {
        const res: any[] = [];
        arr.forEach((item, ix, arr) => {
            const val = func(item, ix, arr);
            if (typeof val === 'undefined' || val === null) {
                return;
            } else if (Array.isArray(val)) {
                res.push(...val);
            } else {
                res.push(val);
            }
        });
        return res;
    }
}
