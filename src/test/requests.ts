import { Requests } from '../ootils'
import * as assert from 'assert';
import * as mocha from 'mocha';

describe('requests', () => {

    describe('get', () => {

        it('gets test data', async () => {
            const res = await Requests.get('http://google.com')
            assert.notEqual(res, null);
        });
    });

});
