import { ProcShell } from '../ootils'
import * as assert from 'assert';
import * as mocha from 'mocha';

describe('procshell', () => {

    describe('spawn', () => {

        it('creates, deletes file', done => {
            const outfile = `test-${Math.floor(Math.random() * 9999)}.txt`;
            const msg = 'Hello World ' + Math.floor(Math.random() * 9999);
            ProcShell.spawn('echo', [msg, '>', outfile]).then(() => {
                ProcShell.spawn('rm', [outfile]).then(done);
            });

        });
    });

    describe('exec', () => {

        it('creates, reads, deletes file', done => {
            const catcher = (err: Error) => assert.equal(err.message, null);
            const outfile = `test-${Math.floor(Math.random() * 9999)}.txt`;
            const msg = 'Hello World ' + Math.floor(Math.random() * 9999);
            ProcShell.exec(['echo', msg, '>', outfile].join(' ')).then(() => {
                return ProcShell.exec(['cat', outfile].join(' '));
            }).then(txt => {
                assert.equal(msg, txt);
                return ProcShell.exec(['rm', outfile].join(' '))
            }).then(done);
        });
    });

});
