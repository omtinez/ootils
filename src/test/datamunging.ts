import { DataMunging } from '../ootils'
import * as assert from 'assert';
import * as mocha from 'mocha';

describe('datamunging', () => {

    describe('readcsv', () => {

        it('todo', done => {
            done();
        });
    });

    describe('#array', () => {

        it('produce', done => {
            const tmp = [[1, 2], [3, 4], [5, 6], [7, 8]];
            const arr1 = DataMunging.produce(tmp, x => x);
            const arr2 = [1, 2, 3, 4, 5, 6, 7, 8];
            assert.deepEqual(arr1, arr2);
            done();
        });
    });
});
