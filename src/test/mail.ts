import { Mail } from '../ootils'
import * as assert from 'assert';
import * as mocha from 'mocha';

describe('mail', () => {

    describe('send', () => {

        it('send dummy email #skipci', async () => {
            const domain = 'mailer.omtinez.com';
            const token = 'key-qwertyuiopasdfghjklzxcvbnm123456';
            const ts = new Date().getTime();

            await Mail.send(token, domain, 'Test <contact@omtinez.com>', 'omtinez@gmail.com',
                `(${ts}) ootils mail test`, `(${ts}) ootils mail test content`);
        });
    });

});
