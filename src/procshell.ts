import * as child_process from 'child_process';
import * as path from 'path';
import * as which_ from 'which';

/** Helper module that contains process and shell-related methods */
export module ProcShell {

    /** Wrapper that promisifies `which` */
    export function which(command: string) {
        return new Promise<string>((resolve, reject) => {
            which_(command, (err: Error, path: string) => {
                if (err) {
                    reject(err);
                } else if (!path) {
                    reject(new Error(`${command} not found`));
                } else {
                    resolve(path);
                }
            });
        });
    }

    /** Wrapper that promisifies `spawn` */
    export function spawn(command: string, args?: string[], opts: {[key: string]: any} = {}) {
        return new Promise<void>((resolve, reject) => {
            ProcShell.which(command).then(command => {
                // Sensible defaults for options
                opts.cwd = opts.hasOwnProperty('cwd') ? opts.cwd : __dirname;
                opts.shell = opts.hasOwnProperty('shell') ? opts.shell : true;
                opts.stdio = opts.hasOwnProperty('stdio') ? opts.stdio : ['ipc'];
                const child = child_process.spawn(command, args, opts);
                child.on('close', code => {
                    if (code !== 0) {
                        reject(new Error(`"${command}" exited with non-zero code ${code}`));
                    } else {
                        resolve();
                    }
                });
            }).catch(err => reject(err));
        });
    }

    /** Wrapper that promisifies `exec` */
    export function exec(command: string, opts: {[key: string]: any} = {}) {
        return new Promise<any>((resolve, reject) => {
            try {
                const opts_: any = {
                    maxBuffer: opts.maxBuffer || 1024 * 500
                }
                const child = child_process.exec(command, opts_, (err: Error, stdout: string, stderr: string) => {
                    if (err) {
                        reject(err);
                    } else if (!opts.ignore_stderr && stderr) {
                        reject(new Error(stderr));
                    } else {
                        resolve(stdout.trim());
                    }
                });
            } catch (ex) {
                reject(new Error(ex.message));
            }
        });
    }

    /** Wrapper that promisifies `execFile` */
    export function file(command: string, args: string[] = [], opts: {[key: string]: any} = {}) {
        return new Promise<any>((resolve, reject) => {
            try {
                const child = child_process.execFile(command, args, (err: Error, stdout: string, stderr: string) => {
                    if (err) {
                        reject(err);
                    } else if (!opts.ignore_stderr && stderr) {
                        reject(new Error(stderr));
                    } else {
                        resolve(stdout.trim());
                    }
                });
            } catch (ex) {
                reject(new Error(ex.message));
            }
        });
    }
}
